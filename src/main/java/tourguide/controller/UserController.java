package tourguide.controller;


import tourguide.entity.User;
import tourguide.service.TourGuideService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    TourGuideService tourGuideService;

    @GetMapping("/getAllUsers")
    public List<User> getAllUsers(){
        return tourGuideService.getAllUsers();
    }
}
