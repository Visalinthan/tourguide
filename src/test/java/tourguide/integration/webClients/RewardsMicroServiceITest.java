package tourguide.integration.webClients;

import org.springframework.boot.test.mock.mockito.MockBean;
import tourguide.exception.UUIDException;
import tourguide.microService.RewardsMicroService;
import tourguide.service.TourGuideService;
import org.apache.commons.lang3.math.NumberUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

@SpringBootTest
//@ActiveProfiles("test")
public class RewardsMicroServiceITest {

    @MockBean
    private RewardsMicroService rewardsMicroService;

    @Autowired
    private TourGuideService tourGuideService;

    @Test
   public void getRewardPointsWebClientShouldReturnFieldsWithValues() throws UUIDException {

        UUID attractionId = new UUID(4872158, 1875147);
        UUID userId = new UUID(41872158, 18175147);
        int rewardPoints = rewardsMicroService.getRewardPointsWebClient(attractionId, userId);
        String rewardPointsString = String.valueOf(rewardPoints);

        Assertions.assertThat(rewardPoints)
                .isNotNull();
        Assertions.assertThat(NumberUtils.isNumber(rewardPointsString)).isTrue();
    }
}
