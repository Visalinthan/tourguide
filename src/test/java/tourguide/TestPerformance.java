package tourguide;

import tourguide.entity.User;
import tourguide.entity.location.Attraction;
import tourguide.entity.location.VisitedLocation;
import tourguide.exception.UUIDException;
import tourguide.helper.InternalTestHelper;
import tourguide.microService.GpsUtilMicroService;
import tourguide.microService.RewardsMicroService;
import tourguide.microService.TripPricerMicroService;
import tourguide.service.InternalTestService;
import tourguide.service.RewardsService;
import tourguide.service.TourGuideService;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestPerformance {

	/*
	 * A note on performance improvements:
	 *
	 *     The number of users generated for the high volume tests can be easily adjusted via this method:
	 *
	 *     		InternalTestHelper.setInternalUserNumber(100000);
	 *
	 *
	 *     These tests can be modified to suit new solutions, just as long as the performance metrics
	 *     at the end of the tests remains consistent.
	 *
	 *     These are performance metrics that we are trying to hit:
	 *
	 *     highVolumeTrackLocation: 100,000 users within 15 minutes:
	 *     		assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
     *
     *     highVolumeGetRewards: 100,000 users within 20 minutes:
	 *          assertTrue(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	 */
  @BeforeAll()
  public static void Setup() {
    Locale.setDefault(new Locale("us"));
  }

    @Test
    public void highVolumeTrackLocation() throws UUIDException, InterruptedException {
        InternalTestService internalTestService = new InternalTestService();
        InternalTestHelper internalTestHelper = new InternalTestHelper();
        // Users should be incremented up to 100,000, and test finishes within 15 minutes
        internalTestHelper.setInternalUserNumber(100);
        GpsUtilMicroService gpsUtilMicroService = new GpsUtilMicroService();
        TripPricerMicroService tripPricerMicroService = new TripPricerMicroService();
        RewardsMicroService rewardsMicroService = new RewardsMicroService();
        RewardsService rewardsService = new RewardsService(gpsUtilMicroService, rewardsMicroService);
        TourGuideService tourGuideService = new TourGuideService(rewardsService, internalTestService,
                gpsUtilMicroService, tripPricerMicroService);

        tourGuideService.tracker.stopTracking();

        //Create a list of UserModel containing all users
        List<User> allUsers = new ArrayList<>();
        allUsers = tourGuideService.getAllUsers();

        //Create a stopWatch and start it
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        tourGuideService.trackListUserLocation(allUsers);
        stopWatch.stop();

        //Asserting part that the time is as performant as wanted
        System.out.println("highVolumeTrackLocation: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds.");
        assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));

    }


    @Test
  public void highVolumeGetRewards() throws InterruptedException {
    // Users should be incremented up to 100,000, and test finishes within 20 minutes

    //Create a stopWatch and start it
    StopWatch stopWatch = new StopWatch();
    stopWatch.start();
    InternalTestService internalTestService = new InternalTestService();
    InternalTestHelper internalTestHelper = new InternalTestHelper();
    internalTestHelper.setInternalUserNumber(100);

    GpsUtilMicroService gpsUtilMicroService = new GpsUtilMicroService();
    TripPricerMicroService tripPricerMicroService = new TripPricerMicroService();
    RewardsMicroService rewardsMicroService = new RewardsMicroService();
    RewardsService rewardsService = new RewardsService(gpsUtilMicroService, rewardsMicroService);
    TourGuideService tourGuideService = new TourGuideService(rewardsService, internalTestService,
      gpsUtilMicroService, tripPricerMicroService);

    tourGuideService.tracker.stopTracking();

    //Create a list of UserModel containing all users
    Attraction attraction = gpsUtilMicroService.getAllAttractionsWebClient().get(0);
    List<User> allUsers = new ArrayList<>();
    allUsers = tourGuideService.getAllUsers();
    tourGuideService.trackListUserLocation(allUsers);
    allUsers.forEach(u -> u.addToVisitedLocations(new VisitedLocation(u.getUserId(), attraction, new Date())));

    allUsers.forEach(u -> rewardsService.calculateRewards(u));

    for(User user : allUsers) {
      assertTrue(user.getUserRewards().size() > 0);
    }
    stopWatch.stop();
    tourGuideService.tracker.stopTracking();

    System.out.println("highVolumeGetRewards: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds.");
    assertTrue(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
  }
}
